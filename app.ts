import express, { response } from 'express';
import appConfig from './appConfig';
import bodyParser from 'body-parser';

import type { Response } from 'express';

const app = express();
const port = 3000;
app.use(bodyParser.json());

app.get('/', (req: Object, res: Response) => {
  res.contentType('text');
  res.send(`
    This is a test project with following assumptions for parse endpoint (v1 and v2)

    -   The client string is expected in the same order of data. (firstName\<delimiter\>lastName\<delimiter\>clientId)
    -   A hyphen to be added as the fourth character in api/v2/parse endpoint
    -   The delimiter in the clientId is to be treated as a part of it and not be separated
  `)
});
appConfig.versions.api.map((version) => {
  app.use(`/api/${version}`, require('./api/routes/' + version).default)
})

export default app;
