import { parseClientString } from '../dataHelpers';

describe('dataHelpers.parseClientString()', () => {
  test('parses client data string', () => {
    const clientDetails = parseClientString('JOHN0000MICHAEL0009994567');
    expect(clientDetails).toEqual({
      firstName: 'JOHN0000',
      lastName: 'MICHAEL000',
      clientId: '9994567'
    })
  })

  test('parses client data string and removes delimiter', () => {
    const clientDetails = parseClientString('JOHN0000MICHAEL0009994567', true);
    expect(clientDetails).toEqual({
      firstName: 'JOHN',
      lastName: 'MICHAEL',
      clientId: '9994567'
    })
  })
})