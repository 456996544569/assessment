/**
 * This method returns the parsed object for the supplied data string
 * the parsed object contains the properties firstName, lastName and clientId
 * @param {string} clientString this is the data string that has to be parsed
 * @returns {Object} the parsed object from the data string
 */
export function parseClientString(clientString: string, removeDelimiter: boolean = false, delimiter: string = '0') {

  const clientDetailsBuffer: Array<string> = [];

  const clientStringSegments = clientString.split(delimiter);
  let i;
  // looping through each segments and finding out the client details
  // The client string must contain the details in the SAME ORDER
  for (i = 0; ((i < clientStringSegments.length) && (!clientDetailsBuffer[2])); i++) {
    if (clientStringSegments[i]) {
      clientDetailsBuffer.push(clientStringSegments[i]);
    } else {
      if (!removeDelimiter) {
        // whetehr to include the delimiter
        clientDetailsBuffer[clientDetailsBuffer.length - 1] += `${delimiter}${(clientStringSegments[i + 1]) ? delimiter : ''}`;
      }
    }
  }

  // to check whether the client id is being split by delimiter
  // this logic can be avoided if assumed that clientid wouldnt contain the delimiter
  if (i + 1 < clientStringSegments.length) {
    clientDetailsBuffer[2] = clientStringSegments.splice(i).join(delimiter);
  }
  return {
    firstName: clientDetailsBuffer[0] || '',
    lastName: clientDetailsBuffer[1] || '',
    clientId: clientDetailsBuffer[2] || ''
  }
}