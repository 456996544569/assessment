import type { Response, Request } from 'express';
import { ClientDetails } from '../../types/ClientDetails';
import { parseClientString } from '../helpers/dataHelpers';

interface ClientStringRequest extends Request {
  data: string;
}

export const clientDataParserV1 = (request: ClientStringRequest, response: Response) => {
  if (request.body.data && typeof request.body.data === 'string') {
    const clientDetails: ClientDetails = parseClientString(request.body.data);
    response.status(200);
    response.json(clientDetails);
  } else {
    response.status(422);
    response.json({ error: 'body.data should be a valid string' });
  }
}

export const clientDataParserV2 = (request: Request, response: Response) => {
  if (request.body.data && typeof request.body.data === 'string') {
    const clientDetails: ClientDetails = parseClientString(request.body.data, true);
    // adding a hyphen at 3rd index
    const formattedClientId = clientDetails.clientId.slice(0, 3) + '-' + clientDetails.clientId.slice(3);
    clientDetails.clientId = formattedClientId;
    response.json(clientDetails);
  } else {
    response.status(422);
    response.json({ error: 'body.data should be a valid string' });
  }
}