const express = require('express');
import { clientDataParserV2 } from '../../controllers/clientController';
var router = express.Router();

router.post('/parse', clientDataParserV2)

export default router;