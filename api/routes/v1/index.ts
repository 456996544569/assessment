const express = require('express');
import { clientDataParserV1 } from '../../controllers/clientController';
var router = express.Router();

router.post('/parse', clientDataParserV1)

export default router;