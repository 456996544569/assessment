export interface ClientDetails {
    firstName: string,
    lastName: string,
    clientId: string
}
