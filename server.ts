import app from "./app";
import appConfig from './appConfig';

const port = appConfig.port || 3000;

app.listen(port, () => {
  console.log(`server is listening on ${port}`);
  console.log(`endpoints
  GET http://localhost:${port}/
  POST http://localhost:${port}/api/v1/parse
  POST http://localhost:${port}/api/v2/parse
  `)
});