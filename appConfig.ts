export default {
  port: 3000,
  versions: {
    api: [
      'v1',
      'v2'
    ]
  }
}