const request = require("supertest");
import { resolvePlugin } from '@babel/core';
import app from '../app';

describe("GET / : Test the root path ", () => {
  test("Server is up and running", done => {
    request(app)
      .get("/")
      .then(response => {
        expect(response.statusCode).toBe(200);
        done();
      });
  });

  test("POST api/v1/parse rejects invalid request body", done => {
    request(app)
      .post("/api/v1/parse", { data: '' })
      .then(response => {
        expect(response.statusCode).toBe(422);
        done();
      });
  })

  test("POST api/v2/parse rejects invalid request body", done => {
    request(app)
      .post("/api/v2/parse", { data: '' })
      .then(response => {
        expect(response.statusCode).toBe(422);
        done();
      });
  })


  test("POST api/v1/parse parses the client data", done => {
    request(app)
      .post("/api/v1/parse")
      .send({ data: 'JOHN0000MICHAEL0009994567' })
      .set('Accept', 'application/json')
      .expect(200)
      .then((response) => {
        expect(response.body).toEqual({
          firstName: 'JOHN0000',
          lastName: 'MICHAEL000',
          clientId: '9994567'
        })
        done();
      });
  })

  test("POST api/v2/parse parses the client data", done => {
    request(app)
      .post("/api/v2/parse", { data: 'JOHN0000MICHAEL0009994567' })
      .send({ data: 'JOHN0000MICHAEL0009994567' })
      .set('Accept', 'application/json')
      .expect(200)
      .then((response) => {
        expect(response.body).toEqual({
          firstName: 'JOHN',
          lastName: 'MICHAEL',
          clientId: '999-4567'
        })
        done();
      });
  })
});